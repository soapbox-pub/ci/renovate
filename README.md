# Soapbox RenovateBot

Runs [renovate-runner](https://gitlab.com/renovate-bot/renovate-runner) every hour.

It scans repos in the soapbox-pub org looking for a renovate.json, and if found, tries to create MRs to update deps.
